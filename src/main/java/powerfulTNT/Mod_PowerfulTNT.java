package powerfulTNT;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.init.Blocks;

@Mod(modid = "PowerfulTNT", name="PowerfulTNT", version = "0.0.0")
public class Mod_PowerfulTNT
{
	@Mod.Instance("PowerfulTNT")
	public static Mod_PowerfulTNT instance;

	@Mod.EventHandler
	/** コマンド登録 **/
	public void serverLoad(FMLServerStartingEvent event){
		event.registerServerCommand(new Command_PowTNT());
	}

	@Mod.EventHandler
	public void preInit(FMLPreInitializationEvent event){
		GameRegistry.registerBlock(new Block_PowTNT(), "PowerfulTNT");
	}

    @EventHandler
    public void init(FMLInitializationEvent event)
    {
		// some example code
        System.out.println("DIRT BLOCK >> "+Blocks.dirt.getUnlocalizedName());
    }
}
