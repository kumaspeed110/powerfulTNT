package powerfulTNT;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.BlockTNT;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.IIcon;
import net.minecraft.world.Explosion;
import net.minecraft.world.World;

public class Block_PowTNT extends BlockTNT
{

	    @SideOnly(Side.CLIENT)
	    private IIcon field_150116_a;
	    @SideOnly(Side.CLIENT)
	    private IIcon field_150115_b;
	    private static final String __OBFID = "CL_00000324";

	    public Block_PowTNT()
	    {
	        super();
			setBlockName("PowerfulTNT");
	    }


	    @Override
	    public void onBlockDestroyedByExplosion(World p_149723_1_, int p_149723_2_, int p_149723_3_, int p_149723_4_, Explosion p_149723_5_)
	    {
	        if (!p_149723_1_.isRemote)
	        {
	            Entity_PowerfulTNT entity_powerfultnt = new Entity_PowerfulTNT(p_149723_1_, (double)((float)p_149723_2_ + 0.5F), (double)((float)p_149723_3_ + 0.5F), (double)((float)p_149723_4_ + 0.5F), p_149723_5_.getExplosivePlacedBy());
	            entity_powerfultnt.fuse = p_149723_1_.rand.nextInt(entity_powerfultnt.fuse / 4) + entity_powerfultnt.fuse / 8;
	            p_149723_1_.spawnEntityInWorld(entity_powerfultnt);
	        }
	    }

	    @Override
	    public void func_150114_a(World p_150114_1_, int p_150114_2_, int p_150114_3_, int p_150114_4_, int p_150114_5_, EntityLivingBase p_150114_6_)
	    {
	        if (!p_150114_1_.isRemote)
	        {
	            if ((p_150114_5_ & 1) == 1)
	            {
	            	Entity_PowerfulTNT entity_powerfultnt = new Entity_PowerfulTNT(p_150114_1_, (double)((float)p_150114_2_ + 0.5F), (double)((float)p_150114_3_ + 0.5F), (double)((float)p_150114_4_ + 0.5F), p_150114_6_);
	                p_150114_1_.spawnEntityInWorld(entity_powerfultnt);
	                p_150114_1_.playSoundAtEntity(entity_powerfultnt, "game.tnt.primed", 1.0F, 1.0F);
	            }
	        }
	    }

	}
