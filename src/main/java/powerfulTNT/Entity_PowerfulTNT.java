package powerfulTNT;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityTNTPrimed;
import net.minecraft.world.World;

public class Entity_PowerfulTNT extends EntityTNTPrimed{
	public static int keisuu = 70000;

	public Entity_PowerfulTNT(World p_i1729_1_) {
		super(p_i1729_1_);
	}

	public Entity_PowerfulTNT(World p_149723_1_, double d, double e, double f, EntityLivingBase explosivePlacedBy) {
		super(p_149723_1_,d,e,f,explosivePlacedBy);
	}

	@Override
    public void onUpdate()
    {
        this.prevPosX = this.posX;
        this.prevPosY = this.posY;
        this.prevPosZ = this.posZ;
        this.motionY -= 0.03999999910593033D;
        this.moveEntity(this.motionX, this.motionY, this.motionZ);
        this.motionX *= 0.9800000190734863D;
        this.motionY *= 0.9800000190734863D;
        this.motionZ *= 0.9800000190734863D;

        if (this.onGround)
        {
            this.motionX *= 0.699999988079071D;
            this.motionZ *= 0.699999988079071D;
            this.motionY *= -0.5D;
        }

        if (this.fuse-- <= 0)
        {
            this.setDead();

            if (!this.worldObj.isRemote)
            {
                this.explode();
            }
        }
        else
        {
            this.worldObj.spawnParticle("smoke", this.posX, this.posY + 0.5D, this.posZ, 0.0D, 0.0D, 0.0D);
        }
    }

    private void explode()
    {
        float f = 4.0F*keisuu;
        this.worldObj.createExplosion(this, this.posX, this.posY, this.posZ, f, true);
    }
}
